<?php

namespace Samy\Environment\Interface;

use InvalidArgumentException;

/**
 * Describes Load interface.
 */
interface LoadInterface
{
    /**
     * Return an instance with loaded data from array.
     *
     * @param array<string,mixed> $Data The load data.
     * @throws InvalidArgumentException If error.
     * @return static
     */
    public function load(array $Data): self;

    /**
     * Return an instance with loaded data from ini file.
     *
     * @param string $Filename The ini filename.
     * @throws InvalidArgumentException If error.
     * @return static
     */
    public function loadIni(string $Filename): self;

    /**
     * Return an instance with loaded data from json file.
     *
     * @param string $Filename The ini filename.
     * @throws InvalidArgumentException If error.
     * @return static
     */
    public function loadJson(string $Filename): self;
}
