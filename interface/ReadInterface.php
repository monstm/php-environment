<?php

namespace Samy\Environment\Interface;

/**
 * Describes Read interface.
 */
interface ReadInterface
{
    /**
     * Retrieve all of environment variables.
     *
     * @return array<string,string>
     */
    public function getEnvironments(): array;

    /**
     * Checks if environment variable exists by the given case-insensitive name.
     *
     * @param string $Name The variable name.
     * @return bool
     */
    public function hasEnvironment(string $Name): bool;

    /**
     * Retrieve string value of environment variable by the given case-insensitive name.
     *
     * @param string $Name The variable name.
     * @param string $Default The default value.
     * @return string
     */
    public function getString(string $Name, string $Default = ""): string;

    /**
     * Retrieve boolean value of environment variable by the given case-insensitive name.
     *
     * @param string $Name The variable name.
     * @param bool $Default The default value.
     * @return bool
     */
    public function getBoolean(string $Name, bool $Default = false): bool;

    /**
     * Retrieve integer value of environment variable by the given case-insensitive name.
     *
     * @param string $Name The variable name.
     * @param int $Default The default value.
     * @return int
     */
    public function getInteger(string $Name, int $Default = 0): int;

    /**
     * Retrieve float value of environment variable by the given case-insensitive name.
     *
     * @param string $Name The variable name.
     * @param float $Default The default value.
     * @return float
     */
    public function getFloat(string $Name, float $Default = 0): float;
}
