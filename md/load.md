# Load

---

## Load Interface

Describes Load interface.

### load

Return an instance with loaded data from array.

```php
$virtual_environment = $virtual_environment->load($data);
```

---

### loadIni

Return an instance with loaded data from ini file.

```php
$virtual_environment = $virtual_environment->loadIni($filename);
```

---

### loadJson

Return an instance with loaded data from json file.

```php
$virtual_environment = $virtual_environment->loadJson($filename);
```
