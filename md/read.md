# Read

---

## Read Interface

Describes Read interface.

### getEnvironments

Retrieve all of environment variables.

```php
$environments = $virtual_environment->getEnvironments();
```

---

### hasEnvironment

Checks if environment variable exists by the given case-insensitive name.

```php
$has_environment = $virtual_environment->hasEnvironment($name);
```

---

### getString

Retrieve string value of environment variable by the given case-insensitive name.

```php
$string = $virtual_environment->getString($name, $default = "");
```

---

### getBoolean

Retrieve boolean value of environment variable by the given case-insensitive name.

```php
$boolean = $virtual_environment->getBoolean($name, $default = false);
```

---

### getInteger

Retrieve integer value of environment variable by the given case-insensitive name.

```php
$integer = $virtual_environment->getInteger($name, $default = 0);
```

---

### getFloat

Retrieve float value of environment variable by the given case-insensitive name.

```php
$float = $virtual_environment->getFloat($name, $default = 0);
```
