# Virtual Environment

This is a simple way to load virtual environment configuration from environment variable or any other environment configuration.

```php
$virtual_environment = new \Samy\Environment\VirtualEnvironment();
```
