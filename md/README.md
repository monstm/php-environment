# PHP Environment

[
	![](https://badgen.net/packagist/v/samy/environment/latest)
	![](https://badgen.net/packagist/license/samy/environment)
	![](https://badgen.net/packagist/dt/samy/environment)
	![](https://badgen.net/packagist/favers/samy/environment)
](https://packagist.org/packages/samy/environment)

The [3rd factor (Store config in the environment)](https://12factor.net/config)
of Twelve-Factor App methodology states that configuration information
should be  kept as environment variables and injected into the application on runtime.
This library lets your application loads variables from environment variables
or any other environment configuration.

---

## Instalation

### Composer

Composer is a tool for dependency management in PHP.
It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.

```sh
composer require samy/environment
```

Please see [Composer](https://getcomposer.org/) for more information.

---

## Support

* Repository: <https://gitlab.com/monstm/php-environment>
* User Manual: <https://monstm.gitlab.io/php-environment/>
* Documentation: <https://monstm.alwaysdata.net/php-environment/>
* Issues: <https://gitlab.com/monstm/php-environment/-/issues>
