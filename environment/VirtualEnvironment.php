<?php

namespace Samy\Environment;

use Samy\Environment\Abstract\AbstractVirtualEnvironment;

/**
 * Simple Virtual Environment implementation.
 */
class VirtualEnvironment extends AbstractVirtualEnvironment
{
    public function __construct()
    {
        $this
            ->load(getenv())
            ->load($_ENV)
            ->load($_SERVER);
    }
}
