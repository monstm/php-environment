<?php

namespace Test\Unit;

use Samy\PhpUnit\AbstractTestCase;
use Test\Inherit\Load;

class LoadTest extends AbstractTestCase
{
    /** @var Load */
    protected $load;

    protected function setUp(): void
    {
        $this->load = new Load();
    }

    /**
     * Test load.
     *
     * @return void
     */
    public function testLoad(): void
    {
        $this->assertInstanceOf(Load::class, $this->load->load($_ENV));
    }

    /**
     * Test load ini.
     *
     * @return void
     */
    public function testLoadIni(): void
    {
        $filename = __DIR__ . DIRECTORY_SEPARATOR . "load.ini";
        $this->assertInstanceOf(Load::class, $this->load->loadIni($filename));
    }

    /**
     * Test load json.
     *
     * @return void
     */
    public function testLoadJson(): void
    {
        $filename = __DIR__ . DIRECTORY_SEPARATOR . "load.json";
        $this->assertInstanceOf(Load::class, $this->load->loadJson($filename));
    }
}
