<?php

namespace Test\Unit;

use Samy\PhpUnit\AbstractTestCase;
use Test\Inherit\Read;

class ReadTest extends AbstractTestCase
{
    /** @var Read */
    protected $read;

    protected function setUp(): void
    {
        $this->read = new Read();
        $this->read->load($_ENV);
    }

    /**
     * Test get environments.
     *
     * @return void
     */
    public function testGetEnvironments(): void
    {
        $this->assertSame($_ENV, $this->read->getEnvironments());
    }

    /**
     * Test has environment.
     *
     * @dataProvider dataProvider
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testHasEnvironment(array $Data): void
    {
        $this->assertSame($Data["has-environment"], $this->read->hasEnvironment(strval($Data["name"])));
    }

    /**
     * Test get string.
     *
     * @dataProvider dataProvider
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testGetString(array $Data): void
    {
        $this->assertSame($Data["string"], $this->read->getString(strval($Data["name"])));
    }

    /**
     * Test get boolean.
     *
     * @dataProvider dataProvider
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testGetBoolean(array $Data): void
    {
        $this->assertSame($Data["boolean"], $this->read->getBoolean(strval($Data["name"])));
    }

    /**
     * Test get integer.
     *
     * @dataProvider dataProvider
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testGetInteger(array $Data): void
    {
        $this->assertSame($Data["integer"], $this->read->getInteger(strval($Data["name"])));
    }

    /**
     * Test get float.
     *
     * @dataProvider dataProvider
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testGetFloat(array $Data): void
    {
        $this->assertSame($Data["float"], $this->read->getFloat(strval($Data["name"])));
    }

    /**
     * Retrieve data provider.
     *
     * @return array<string,mixed>
     */
    public static function dataProvider(): array
    {
        $ret = [];

        foreach (self::csv(__DIR__ . DIRECTORY_SEPARATOR . "read.csv") as $data) {
            $id = $data["id"];
            $ret["read-" . $id] = [[
                "name" => $data["name"],
                "has-environment" => ($data["has-environment"] == "true"),
                "string" => $data["string"],
                "boolean" => ($data["boolean"] == "true"),
                "integer" => intval($data["integer"]),
                "float" => floatval($data["float"]),
            ]];
        }

        return $ret;
    }
}
