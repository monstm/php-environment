<?php

namespace Samy\Environment\Abstract;

/**
 * This is a simple Virtual Environment implementation that other Virtual Environment can inherit from.
 */
abstract class AbstractVirtualEnvironment extends AbstractRead
{
}
