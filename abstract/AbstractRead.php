<?php

namespace Samy\Environment\Abstract;

use Samy\Environment\Interface\ReadInterface;

/**
 * This is a simple Read implementation that other Read can inherit from.
 */
abstract class AbstractRead extends AbstractLoad implements ReadInterface
{
    /**
     * Retrieve all of environment variables.
     *
     * @return array<string,string>
     */
    public function getEnvironments(): array
    {
        return $this->environments;
    }

    /**
     * Checks if environment variable exists by the given case-insensitive name.
     *
     * @param string $Name The variable name.
     * @return bool
     */
    public function hasEnvironment(string $Name): bool
    {
        $key = $this->getKey($Name);
        return isset($this->environments[$key]);
    }

    /**
     * Retrieve string value of environment variable by the given case-insensitive name.
     *
     * @param string $Name The variable name.
     * @param string $Default The default value.
     * @return string
     */
    public function getString(string $Name, string $Default = ""): string
    {
        $key = $this->getKey($Name);
        return $this->environments[$key] ?? $Default;
    }

    /**
     * Retrieve boolean value of environment variable by the given case-insensitive name.
     *
     * @param string $Name The variable name.
     * @param bool $Default The default value.
     * @return bool
     */
    public function getBoolean(string $Name, bool $Default = false): bool
    {
        $value = $this->getString($Name, ($Default ? "true" : "false"));
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    /**
     * Retrieve integer value of environment variable by the given case-insensitive name.
     *
     * @param string $Name The variable name.
     * @param int $Default The default value.
     * @return int
     */
    public function getInteger(string $Name, int $Default = 0): int
    {
        return intval($this->getString($Name, strval($Default)));
    }

    /**
     * Retrieve float value of environment variable by the given case-insensitive name.
     *
     * @param string $Name The variable name.
     * @param float $Default The default value.
     * @return float
     */
    public function getFloat(string $Name, float $Default = 0): float
    {
        return floatval($this->getString($Name, strval($Default)));
    }
}
