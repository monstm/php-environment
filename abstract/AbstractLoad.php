<?php

namespace Samy\Environment\Abstract;

use InvalidArgumentException;
use Samy\Environment\Interface\LoadInterface;

/**
 * This is a simple Load implementation that other Load can inherit from.
 */
abstract class AbstractLoad implements LoadInterface
{
    /** @var array<string,string> */
    protected $environments = [];

    /**
     * Retrieve normalized variable key.
     *
     * @param string $Name The variable name.
     * @return string
     */
    protected function getKey(string $Name): string
    {
        return strtoupper(trim($Name));
    }

    /**
     * Return an instance with loaded data from array.
     *
     * @param array<string,mixed> $Data The load data.
     * @throws InvalidArgumentException If error.
     * @return static
     */
    public function load(array $Data): self
    {
        foreach ($Data as $name => $value) {
            if (!is_string($name)) {
                throw new InvalidArgumentException("Unexpected key[" . strval($name) . "] in data.");
            }

            $key = $this->getKey($name);
            if (isset($this->environments[$key])) {
                continue;
            }

            $type = strtolower(gettype($value));
            switch ($type) {
                case "string":
                case "integer":
                case "double":
                    $this->environments[$key] = strval($value);
                    break;
                case "boolean":
                    $this->environments[$key] = ($value ? "true" : "false");
                    break;
            }
        }

        return $this;
    }

    /**
     * Return an instance with loaded data from ini file.
     *
     * @param string $Filename The ini filename.
     * @throws InvalidArgumentException If error.
     * @return static
     */
    public function loadIni(string $Filename): self
    {
        if (!is_file($Filename)) {
            throw new InvalidArgumentException("The file '" . $Filename . "' is not exists");
        }

        $data = parse_ini_file($Filename);
        if (!is_array($data)) {
            throw new InvalidArgumentException("Invalid ini file '" . $Filename . "' format");
        }

        return $this->load($data);
    }

    /**
     * Return an instance with loaded data from json file.
     *
     * @param string $Filename The ini filename.
     * @throws InvalidArgumentException If error.
     * @return static
     */
    public function loadJson(string $Filename): self
    {
        if (!is_file($Filename)) {
            throw new InvalidArgumentException("The file '" . $Filename . "' is not exists");
        }

        $content = file_get_contents($Filename);
        if (!is_string($content)) {
            throw new InvalidArgumentException("Failed to open file '" . $Filename . "' stream");
        }

        $data = json_decode($content, true);
        if (!is_array($data)) {
            throw new InvalidArgumentException(
                "The file '" . $Filename . "': " .
                    strval(json_last_error()) . " - " . json_last_error_msg()
            );
        }

        return $this->load($data);
    }
}
